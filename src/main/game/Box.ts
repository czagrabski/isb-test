import {BoxView} from "./view/BoxView";
import {AbstractComponent} from "../../base/AbstractComponent";
import {BoxController} from "./controller/BoxController";


export class Box extends AbstractComponent {

    protected createViews(): void {
        this.addView("BoxView", new BoxView());
    }

    protected createControllers():void {
        this.addController("BoxView", new BoxController());
    }
}