import {ShapeDecorator} from "../decorators/ShapeDecorator";

export class Shape {

    private _shapeContainer: PIXI.Container;
    public get shape(): PIXI.Container { return this._shapeContainer; }
    private _shapeDec: ShapeDecorator;

    public fallingSpeed: number = 0;

    constructor() {
        this.createContainer();
        this.createDecorator();
    }

    private createContainer(): void {
        this._shapeContainer = new PIXI.Container;
    }

    public createDecorator(): void {
        this._shapeDec = new ShapeDecorator();
        this._shapeContainer.addChild(this._shapeDec.shapeGraphic);
    }

    public reset(): void {
        this.fallingSpeed = 0;
        this.redecorateShape();
        this._shapeContainer.addChild(this._shapeDec.shapeGraphic);
    }

    public redecorateShape(): void {
        this._shapeContainer.removeChild(this._shapeDec.shapeGraphic);
        this._shapeDec.reset();
    }
}