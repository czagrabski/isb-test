export class ButtonNames {
    public static PLUS_GRAVITY: string = "plusGravity";
    public static MINUS_GRAVITY: string = "minusGravity";
    public static PLUS_AMOUNT: string = "plusAmount";
    public static MINUS_AMOUNT: string = "minusAmount";
}