import {Logger} from "../../../../utils/logger";

export class Pool<T>  {

    private pool: T[];

    constructor() {
        this.pool = [];
    }

    public addItem(item: T): T {
        this.pool.push(item);
        return item;
    }

    public shift(): T {
        return this.pool.shift();
    }

    public pop(): T {
        return this.pool.pop();
    }

    public getRandomItem(): T {
        return this.pool.splice(Math.floor(Math.random() * this.pool.length), 1)[0];
    }

    public get length(): number {
        return  this.pool.length;
    }

}