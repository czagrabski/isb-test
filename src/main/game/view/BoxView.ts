import {AbstractView} from "../../../base/view/AbstractView";
import {Logger} from "../../../utils/logger";
import {Shape} from "./elements/Shape";
import {Pool} from "./misc/Pool";
import _ = require("lodash");
import {HTMLButton} from "../../../base/misc/HTMLButton";
import {ButtonNames} from "./misc/ButtonNames";

export class BoxView extends AbstractView {
    private _mask: PIXI.Graphics;
    private _boxContainer: PIXI.Container;
    private _box: PIXI.Graphics;

    private _shapesPool: Pool<Shape>;
    private _shapesNumberPool: number = 20;

    private _shapesPerSecond: number = 3;
    private _timeSinceLastShape: number = 0;
    private _shapesOnScreen: Shape[];

    private _gravity: number = 0.01;

    private _plusGravityBtn: HTMLButton;
    private _minusGravityBtn: HTMLButton;
    private _plusAmountBtn: HTMLButton;
    private _minusAmountBtn: HTMLButton;
    private _gravityText: PIXI.Text;
    private _amountText: PIXI.Text;

    protected initilize(): void {
        this.createMaskAndBox();
        this.createShapesPool();
        this.createButtons();
        this.createTexts();
    }

    private createMaskAndBox(): void {
        this._boxContainer = new PIXI.Container();
        this.container.addChild(this._boxContainer);
        this._boxContainer.position.set(10, 50);
        this._box = new PIXI.Graphics();
        this._box.beginFill(0x002020);
        this._box.drawRect(0,0, 400, 200);
        this._boxContainer.addChild(this._box);

        this._mask = new PIXI.Graphics();
        this._mask.beginFill(0xFFFFFF);
        this._mask.drawRect(this._boxContainer.x,this._boxContainer.y, this._box.width, this._box.height);
        this.container.addChild(this._mask);

        this._boxContainer.mask = this._mask;
    }

    private createShapesPool(): void {
        this._shapesPool = new Pool<Shape>();
        for(let i:number = 0; i < this._shapesNumberPool; i++) {
            this._shapesPool.addItem(new Shape());
        }
    }

    private createButtons(): void {
        this._plusGravityBtn = new HTMLButton(document.querySelector("#" + ButtonNames.PLUS_GRAVITY), ButtonNames.PLUS_GRAVITY);
        this._minusGravityBtn = new HTMLButton(document.querySelector("#" + ButtonNames.MINUS_GRAVITY), ButtonNames.MINUS_GRAVITY);
        this._plusGravityBtn.onClick.add(this.onGravityClick, this);
        this._minusGravityBtn.onClick.add(this.onGravityClick, this);

        this._plusAmountBtn = new HTMLButton(document.querySelector("#" + ButtonNames.PLUS_AMOUNT), ButtonNames.PLUS_AMOUNT);
        this._minusAmountBtn = new HTMLButton(document.querySelector("#" + ButtonNames.MINUS_AMOUNT), ButtonNames.MINUS_AMOUNT);
        this._plusAmountBtn.onClick.add(this.onAmountClick, this);
        this._minusAmountBtn.onClick.add(this.onAmountClick, this);
    }

    private createTexts(): void {
        this._gravityText = new PIXI.Text("Gravity: " + this._gravity.toString());
        this._gravityText.position.x = 10;
        this._gravityText.position.y = 320;
        this._gravityText.style.fill = 0xFFFFFF;
        this.container.addChild(this._gravityText);

        this._amountText = new PIXI.Text("Shapes per second: " + this._shapesPerSecond.toString());
        this._amountText.position.x = 10;
        this._amountText.position.y = 350;
        this._amountText.style.fill = 0xFFFFFF;
        this.container.addChild(this._amountText);
    }

    private onGravityClick(name: string): void {
        let mod: number;
        if(name == ButtonNames.PLUS_GRAVITY) mod = 0.01;
        if(name == ButtonNames.MINUS_GRAVITY) mod = -0.01;
        if(mod == -0.01 && this._gravity + mod <= 0) {
            this._gravity = 0;
        } else {
            this._gravity += mod;
        }
        this._gravity = Math.ceil(this._gravity * 100) / 100;
        this._gravityText.text = "Gravity: " + this._gravity.toString();
    }

    private onAmountClick(name: string): void {
        let mod: number;
        if(name == ButtonNames.PLUS_AMOUNT) mod = 1;
        if(name == ButtonNames.MINUS_AMOUNT) mod = -1;
        if(mod == -1 && this._shapesPerSecond == 1) mod = 0;
        this._shapesPerSecond += mod;
        this._amountText.text = "Shapes per second: " + this._shapesPerSecond.toString();
    }

    protected start(): void {
        this._shapesOnScreen = [];
        requestAnimationFrame(() => this.onFrame() );

    }

    private onFrame(): void {
        // Logger.log(PIXI.ticker.shared.elapsedMS);
        this._timeSinceLastShape += PIXI.ticker.shared.elapsedMS;
        if(this._timeSinceLastShape > this.shapesDelay) {
            this.addShapeToScreen();
            this._timeSinceLastShape = 0;
        }

        this.animateShapes();

        requestAnimationFrame(()=>this.onFrame());
    }

    private addShapeToScreen(): void {
        if(this._shapesPool.length == 0) {
            this._shapesPool.addItem(new Shape());
        }
        let random: Shape = this._shapesPool.getRandomItem();
        this._boxContainer.addChild(random.shape);
        this.randomizePosition(random.shape);

        this._shapesOnScreen.push(random);
    }

    private animateShapes(): void {
        _.forEach(this._shapesOnScreen, (shape, index) => {
            if(!_.isNil(shape)) {
                shape.shape.y += shape.fallingSpeed;
                this.checkIfUtilizeShape(shape);
                shape.fallingSpeed += this._gravity;
            }
        });
    }

    private checkIfUtilizeShape(shape: Shape): void {
        if(shape.shape.y > this._boxContainer.y + this._box.height + shape.shape.height/2) {
            this._boxContainer.removeChild(shape.shape);
            shape.reset();
            this._shapesPool.addItem(shape);
            this._shapesOnScreen.splice(this._shapesOnScreen.indexOf(shape), 1);
        }
    }

    private randomizePosition(shape: PIXI.Container): void {
        shape.x = _.random(shape.width/2, this._box.width-shape.width/2);
        shape.y = -50;
    }

    private get shapesDelay():number {
        return 1000/this._shapesPerSecond;
    }
}