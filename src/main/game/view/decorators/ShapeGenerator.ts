import PI_2 = PIXI.PI_2;

export class ShapeGenerator {

    public static drawStar(x: number, y: number, points: number, radius:number, innerRadius:number = 0, rotation: number = 0): number[] {
        innerRadius = innerRadius == 0 ? radius / 2 : innerRadius;
        const startAngle:number = (-1 * Math.PI / 2) + rotation;
        const len: number = points * 2;
        const delta: number = PI_2 / len;
        const polygon: number[] = [];

        for (let i = 0; i < len; i++)
        {
            const r: number = i % 2 ? innerRadius : radius;
            const angle: number = (i * delta) + startAngle;
            polygon.push(
                x + (r * Math.cos(angle)),
                y + (r * Math.sin(angle))
            );
        }

        return polygon;
    }
}