import {enumCount} from "../../../../utils/enumCount";
import {Logger} from "../../../../utils/logger";
import {ShapeGenerator} from "./ShapeGenerator";
import _ = require("lodash");

export enum ShapeTypes {
    TRIANGLE,
    RECTANGLE,
    // FIVESIDE,
    // SIXSIDE,
    CIRCLE,
    ELLIPSE,
    RANDOM
}

export class ShapeDecorator {

    private _shapeGraphic: PIXI.Graphics;
    public get shapeGraphic(): PIXI.Graphics { return this._shapeGraphic; }

    private _myShape: number;

    constructor() {
        this.createShapeGraphic();
    }

    private createShapeGraphic(): void {
        this._myShape = Math.floor(Math.random() * enumCount(ShapeTypes));
        // Logger.log(this._myShape.toString());

        this._shapeGraphic = new PIXI.Graphics;
        this._shapeGraphic.beginFill(this.getRandomColor());

        let polygon: number[];
        switch (this._myShape) {
            case ShapeTypes.TRIANGLE:
                polygon = ShapeGenerator.drawStar(0,0,3,
                    _.random(20,50),0,Math.random()*Math.PI);

                this._shapeGraphic.drawPolygon(polygon);
                break;

            case ShapeTypes.RECTANGLE:
                polygon = ShapeGenerator.drawStar(0,0,2,
                    _.random(20,50),0,Math.random()*Math.PI);

                this._shapeGraphic.drawPolygon(polygon);
                break;

            // case ShapeTypes.FIVESIDE:
            // case ShapeTypes.SIXSIDE:
            case ShapeTypes.CIRCLE:
                this._shapeGraphic.drawCircle(0, 0, _.random(20,50));
                break;

            case ShapeTypes.ELLIPSE:
                this._shapeGraphic.drawEllipse(0,0, _.random(20,50),  _.random(20,50));
                break;

            case ShapeTypes.RANDOM:
                polygon = ShapeGenerator.drawStar(0,0,Math.floor(Math.random()*10)+2,
                    _.random(20,50),0,Math.random()*Math.PI);

                this._shapeGraphic.drawPolygon(polygon);
                break;

            default: break;
        }
    }

    private getRandomColor(): number {
        return Math.floor(Math.random()*14777214) + 1;
    }

    public reset(): void {
        this._shapeGraphic.destroy();
        this.createShapeGraphic();
    }
}