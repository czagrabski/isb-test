import {AbstractController} from "../../../base/controller/AbstractController";
import {BoxView} from "../view/BoxView";

export class BoxController extends AbstractController {

    public get view(): BoxView {
        return super.getView() as BoxView;
    }

    protected start(): void {
        this.view.initilizeView.dispatch();
        this.view.startView.dispatch();
    }
}