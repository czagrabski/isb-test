import "pixi.js";
import {EntryPoint} from "./main/EntryPoint";

window.onload = () => {
    EntryPoint.start();
};