export class Logger {
    public static isDebug: boolean = true;
    public static log(msg: any):void {
        if(this.isDebug) {
            console.log(msg);
        }
    }
}