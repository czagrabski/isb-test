export function enumCount(e: any) {
    let count = 0;
    Object.keys(e).forEach((val, idx) => {
        if (Number(isNaN(Number(val)))) {
            count++;
        }
    });
    return count;
}