import {IView} from "./view/IView";
import {IController} from "./controller/IController";

export class AbstractComponent {

    private _viewDict: Dictionary<IView>;
    public getView(identifier: string): IView {
        return this._viewDict[identifier];
    }

    private _controllerDict: Dictionary<IController>;
    public getController(identifier: string): IController {
        return this._controllerDict[identifier];
    }

    constructor() {
        this.createViews();
        this.createControllers();
    }

    protected createViews(): void {}
    protected addView(identifier: string, view: IView): void {
        if(!this._viewDict) {
            this._viewDict = {};
        }
        this._viewDict[identifier] = view;
    }

    protected createControllers(): void {}
    protected addController(identifier: string, controller: IController): void {
        if(!this._controllerDict) {
            this._controllerDict = {};
        }
        this._controllerDict[identifier] = controller;
        controller.setView(this.getView(identifier));
        controller.initController();
    }
}