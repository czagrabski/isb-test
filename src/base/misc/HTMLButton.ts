import {Signal} from "signals";

export class HTMLButton {

    private _name: string;

    public onClick: Signal = new Signal();

    constructor(e: Element, name: string) {
        this.addClickListener(e);
        this._name = name;
    }

    private addClickListener(e: Element): void {
        e.addEventListener("click", ()=> {
            this.onClick.dispatch(this._name);
        });
    }
}