import pixiManager from "utils/pixi-manager";
import {IView} from "./IView";
import {Signal} from "signals";

export class AbstractView implements IView {
    private _container: PIXI.Container;
    public get container():PIXI.Container { return this._container; }

    public startView: Signal = new Signal();;
    public initilizeView: Signal = new Signal();;

    constructor() {
        this.initContainer();
        this.initBaseSignals();
        this.initAdditionalSignals();
    }

    protected initContainer(): void {
        this._container = new PIXI.Container();
        pixiManager.stage.addChild(this._container);
    }

    private initBaseSignals(): void {
        this.startView.add(this.start, this);
        this.initilizeView.add(this.initilize, this);
    }

    private initAdditionalSignals(): void {}

    protected start(): void {}
    protected initilize(): void {}
}