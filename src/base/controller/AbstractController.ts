import {IController} from "./IController";
import {IView} from "../view/IView";
import {Signal} from "signals";

export class AbstractController implements IController {
    private _myView: IView;

    private _startController: Signal = new Signal();

    constructor() {
        this.initBaseSignals();
    }

    private initBaseSignals(): void {
        this._startController.add(this.start, this);
    }

    public initController(): void {
        this._startController.dispatch();
    }

    protected start(): void {
    }

    public setView(view: IView): void {
        this._myView = view;
    }
    protected getView(): IView { return this._myView; }
}