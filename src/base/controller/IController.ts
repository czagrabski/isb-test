import {IView} from "../view/IView";

export interface IController {
    setView(view: IView): void;
    initController(): void;
}